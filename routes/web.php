<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Product;
use App\Categorie;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

//  *CRUD PRODUCTS
//ingreso a vista insercion product - traer tablas info products
Route::get('/products', function () {
	$products = product::all();
   return view('products', [ 'products' => $products ]);
});

//ingreso a vista edicion product - traer info product a editar
Route::get('/products/{id}', function ($id) {
	$product = product::findOrFail($id);
   return view('product-editar', [ 'product' => $product ]);
});


//insercion product
Route::post('/product', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric',
            'name' => 'required|min:3|max:50',
            'quantity' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect('/products')
                ->withInput()
                ->withErrors($validator);
        }

        $product = new product;
        $product->category_id = $request->category_id;
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->save();
        return redirect('/products');
});

//edicion product
Route::put('/product/{id}', function (Request $request, $id) {
    $validator = Validator::make($request->all(), [
        'category_id' => 'required|numeric',
        'name' => 'required|min:3|max:50',
        'quantity' => 'required|numeric'
    ]);

        if ($validator->fails()) {
            return redirect('/products')
                ->withInput()
                ->withErrors($validator);
        }

        $product = product::findOrFail($id);

        $product->category_id = $request->category_id;
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->save();

        return redirect('/products');
});

//borrar product
Route::delete('/product/{id}', function ($id) {
        product::findOrFail($id)->delete();
        return redirect('products');
});

//  *CRUD CATEGORIES
//ingreso a vista insercion categorie - traer tablas info categories
Route::get('/categories', function () {
	$categories = categorie::all();
   return view('categories', [ 'categories' => $categories ]);
});

//ingreso a vista edicion categorie - traer info categorie a editar
Route::get('/categories/{id}', function ($id) {
	$categorie = categorie::findOrFail($id);
   return view('categorie-editar', [ 'categorie' => $categorie ]);
});


//insercion categorie
Route::post('/categorie', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/categories')
                ->withInput()
                ->withErrors($validator);
        }

        $categorie = new categorie;
        $categorie->name = $request->name;
        $categorie->save();
        return redirect('/categories');
});

//edicion categorie
Route::put('/categorie/{id}', function (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/categories')
                ->withInput()
                ->withErrors($validator);
        }

        $categorie = categorie::findOrFail($id);

        $categorie->name = $request->name;
        $categorie->save();

        return redirect('/categories');
});

//borrar categorie
Route::delete('/categorie/{id}', function ($id) {
        categorie::findOrFail($id)->delete();
        return redirect('categories');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
