<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class productsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert(
            [
                'category_id' => '1',
                'name'  => 'laptop',
                'quantity'  => '20',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        DB::table('products')->insert(
            [
                'category_id' => '2',
                'name'  => 'mouse',
                'quantity'  => '30',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        DB::table('products')->insert(
            [
                'category_id' => '3',
                'name'  => 'computer',
                'quantity'  => '15',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
            );

        DB::table('products')->insert(
            [
                'category_id' => '4',
                'name'  => 'desk',
                'quantity'  => '60',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
            );
    }
}
