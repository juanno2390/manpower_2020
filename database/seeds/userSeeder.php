<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            [
                'name'  => 'manpower',
                'email' => 'manpower@gmail.com',
                //password 12345678
                'password' => '$2y$10$R8SuzZf2D25AXIT2iTqLU.cdNPZ18UnpONgGAPAQogYinU8c88d3G',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
    }
}
