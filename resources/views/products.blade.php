@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel-title">
            <h1>products</h1>
        </div>
        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <!-- New Task Form -->
            <form action="{{ url('product') }} " method="POST">
                {{ csrf_field() }}

                <!-- Task Name -->
                <div class="form-group">
                    <label for="category_id" class="control-label">category_id</label>
                    <input type="text" name="category_id" class="form-control">
                </div>

                <!-- Task Name -->
                <div class="form-group">
                    <label for="name" class="control-label">Name</label>
                    <input type="text" name="name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="quantity" class="control-label">quantity</label>
                    <input type="text" name="quantity" class="form-control">
                </div>

                <!-- Add Task Button -->
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Registrar product
                    </button>
                </div>
            </form>
        </div>
    </div>

<div class="col-md-12">
    <!-- Tabla -->
        @if (count($products) > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de products
                </div>

                <div class="panel-body">
                    <table class="table table-striped task-table">
                        <thead>
                            <th>Category_id</th>
                            <th>Name</th>
                            <th>quantity</th>
                            <th>Acción</th>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td class="table-text"><div>{{ $product->category_id }}</div></td>
                                <td class="table-text"><div>{{ $product->name }}</div></td>
                                <td class="table-text"><div>{{ $product->quantity }}</div></td>

                                <td>
                                    <button type="submit" class="btn btn-primary" onclick="location.href='products/{{ $product->id }}'">
                                        <i class="fa fa-pencil"></i>Editar
                                    </button>

                                    <form action="{{ url('product') }}/{{ $product->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>Borrar
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
</div>
    <!-- TODO: Current Tasks -->
@endsection
