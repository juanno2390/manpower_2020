@extends('layouts.app')

@section('content')
<div class="col-sm-offset-3 col-sm-6">
    <div class="panel-title">
        <h1>products</h1>
    </div>
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')


        <form action="{{ url('product') }}/{{ $product->id }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group">
                <label for="category_id" class="control-label">category_id</label>
                <input type="text" name="category_id" class="form-control" value="{{ $product->category_id }}">
            </div>

            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" name="name" class="form-control" value="{{ $product->name }}">
            </div>

            <div class="form-group">
                <label for="quantity" class="control-label">quantity</label>
                <input type="text" name="quantity" class="form-control" value="{{ $product->quantity }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-plus"></i> Editar product
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
