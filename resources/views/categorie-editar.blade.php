@extends('layouts.app')

@section('content')
<div class="col-sm-offset-3 col-sm-6">
    <div class="panel-title">
        <h1>categories</h1>
    </div>
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')


        <form action="{{ url('categorie') }}/{{ $categorie->id }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input type="text" name="name" class="form-control" value="{{ $categorie->name }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-plus"></i> Editar categorie
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
