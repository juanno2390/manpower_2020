@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel-title">
            <h1>categories</h1>
        </div>
        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <!-- New Task Form -->
            <form action="{{ url('categorie') }} " method="POST">
                {{ csrf_field() }}

                <!-- Task Name -->
                <div class="form-group">
                    <label for="name" class="control-label">Name</label>
                    <input type="text" name="name" class="form-control">
                </div>

                <!-- Add Task Button -->
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Registrar categorie
                    </button>
                </div>
            </form>
        </div>
    </div>

<div class="col-md-12">
    <!-- Tabla -->
        @if (count($categories) > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de categories
                </div>

                <div class="panel-body">
                    <table class="table table-striped task-table">
                        <thead>
                            <th>Name</th>
                            <th>Acción</th>
                        </thead>
                        <tbody>
                        @foreach ($categories as $categorie)
                            <tr>
                                <td class="table-text"><div>{{ $categorie->name }}</div></td>

                                <td>
                                    <button type="submit" class="btn btn-primary" onclick="location.href='categories/{{ $categorie->id }}'">
                                        <i class="fa fa-pencil"></i>Editar
                                    </button>

                                    <form action="{{ url('categorie') }}/{{ $categorie->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>Borrar
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
</div>
    <!-- TODO: Current Tasks -->
@endsection
